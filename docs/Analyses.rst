Analyses
==========
The Analyses object represents the Analyses tab in the Chemotion user interface.
It contains all measurement data and metadata associated with the elements

API Classes
------------

.. autoclass:: chemotion_api.elements.abstract_element.AnalysesList
    :members:

.. autoclass:: chemotion_api.elements.abstract_element.Analyses
    :members:

.. autoclass:: chemotion_api.elements.abstract_element.Dataset
    :members: