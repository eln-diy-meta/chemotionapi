Collection
==========


Collections are the main tool given by the Chemotion system to organize elements.
In the Python APi Collection are seperated in different classes. The root collection
is the basic element to work with collections.


.. autoclass:: chemotion_api.collection.RootCollection
    :members:
    :inherited-members:

The root collection organizes the standard Collections:

.. autoclass:: chemotion_api.collection.Collection
    :members:
    :inherited-members:

.. autoenum:: chemotion_api.collection.SyncPermission

The RootCollection as also a property to get the RootSyncCollection

.. autoclass:: chemotion_api.collection.RootSyncCollection