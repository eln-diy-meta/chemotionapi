Chemotion Instance
==================

The chemotion instance is the foundation of the
the python chemtion client. It includes all methods to work with this tool.

.. autoclass:: chemotion_api.Instance
    :members: