Research Plan
===============

.. autoclass:: chemotion_api.elements.research_plan.ResearchPlan
    :members:

.. autoclass:: chemotion_api.elements.research_plan.Table
    :members: