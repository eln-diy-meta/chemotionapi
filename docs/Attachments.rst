Attachments
================

This object handles the attachments attached to a element

API Classes
------------

.. autoclass:: chemotion_api.elements.attachments.Attachments
    :members:

.. autoclass:: chemotion_api.elements.attachments.Attachment
    :members: