Sample
==========

.. autoclass:: chemotion_api.elements.sample.Sample
    :members:


.. autoclass:: chemotion_api.elements.sample.Molecule
    :members:



.. autoclass:: chemotion_api.elements.sample.SolventList
    :members:
