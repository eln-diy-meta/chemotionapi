import pytest

from chemotion_api import Instance
def test_sliceing(instance_with_test_samples):
    instance: Instance = instance_with_test_samples['instance']
    mc = instance_with_test_samples['main_collection']
    samples = mc.get_samples()[:2]
    assert len(samples) == 2
    try:
        mc.get_samples()[-1]
        assert False
    except:
        assert True
    try:
        mc.get_samples()[:-1]
        assert False
    except:
        assert True
def test_new_generics(instance_with_test_samples):
    instance: Instance = instance_with_test_samples['instance']

    mc = instance_with_test_samples['main_collection']
    dev = mc.new_element_by_iri('chmotion:generic_type/try/1.0')
    nc = mc.new_element_by_iri(dev.json_ld['@type'])
    assert dev.json_ld['@type'] == 'chmotion:generic_type/try/1.0'
    assert dev.json_ld['@type'] == nc.json_ld['@type']
    nc.save()
    assert f'http://0.0.0.0:3579/api/v1/generic_elements/{nc.id}.json' == nc.json_ld['@id']
    rc = instance.get_root_collection()
    per_page = 3
    idx = 4
    s4 = rc.get_samples(per_page)[idx]
    assert 'chmotion:type/sample/1.10.5' == s4.json_ld['@type']
    assert f'http://0.0.0.0:3579/api/v1/samples/{s4.id}.json' == s4.json_ld['@id']
    for i, s in enumerate(rc.get_elements_of_iri(s4.json_ld['@type'])):
        if i == idx:
            assert s == s4
            assert not s != s4
        else:
            assert not s == s4
            assert s != s4

        assert s4.json_ld['@type'] == s.json_ld['@type']
    for s in mc.get_elements_of_iri(dev.json_ld['@type']):
        assert s == nc