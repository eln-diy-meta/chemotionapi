import os

import pytest

from chemotion_api.elements.reaction import QuantityUnit


@pytest.fixture()
def instance_with_test_reaction(instance_with_test_samples):
    ra = instance_with_test_samples['main_collection'].new_reaction()
    s1 = instance_with_test_samples['instance'].get_sample(instance_with_test_samples.get('sample_ids')[0])
    ra.properties['starting_materials'].append(s1)
    ana = ra.analyses
    ana.add_analyses('T1')
    data_dir = os.path.join(os.path.dirname(__file__), 'data')
    ana.add_analyses('T2').add_dataset(os.path.join(data_dir, 'SG-V3545-6-13.cif'))
    ra.save()
    yield instance_with_test_samples | {
        'reaction': ra
    }

def test_ana_variation(instance_with_test_reaction):
    ra = instance_with_test_reaction['reaction']
    ra.variations.add_new()
    ana = ra.analyses.by_name("T1")
    ra.variations[0].link_analyses(ana)

    ra.save(True)

    assert [a['name'] for a in ra.variations[0].get_analyses()] == ['T1']

def test_reaction_variants(instance_with_test_reaction):
    ra = instance_with_test_reaction['instance'].get_reaction(1)
    assert len(ra.variations) == 2
    variant = ra.variations[0]
    assert variant.properties == {'duration': {'unit': 'Second(s)', 'value': 2},
                                  'temperature': {'unit': '°C', 'value': 1}}
    assert [x.sample.id for x in variant.starting_materials] == [2]
    assert [x.sample.id for x in variant.reactants] == [3]
    assert [x.sample.id for x in variant.products] == [4]


def test_add_variations(instance_with_test_reaction):
    ra = instance_with_test_reaction['instance'].get_reaction(1)
    ra.variations.add_new()
    ra.save(True)

    assert len(ra.variations) == 3
    variant = ra.variations[-1]
    assert variant.properties == {'duration': {'unit': 'Second(s)', 'value': 0},
                                  'temperature': {'unit': '°C', 'value': 0}}
    variant.starting_materials[0].set_quantity(2, QuantityUnit.Gram)
    ra.save(True)
    assert variant.starting_materials[0].potential_units() == [QuantityUnit.Mole, QuantityUnit.Gram]
    assert variant.reactants[0].potential_units() ==  [QuantityUnit.Mole, QuantityUnit.Gram, QuantityUnit.Equivalent]
    assert variant.products[0].potential_units() ==  [QuantityUnit.Mole, QuantityUnit.Gram]
    assert [x.sample.id for x in variant.starting_materials] == [2]
    assert [x.clean() for x in variant.starting_materials] == [{'amount': {'unit': 'mol', 'value': 0.11743622625733097},
                                                                'aux': {'coefficient': 1.0,
                                                                        'equivalent': 0.0,
                                                                        'isReference': True,
                                                                        'loading': None,
                                                                        'molarity': 0.0,
                                                                        'molecularWeight': 17.03052,
                                                                        'purity': 1.0,
                                                                        'sumFormula': 'H3N',
                                                                        'yield': None},
                                                                'mass': {'unit': 'g', 'value':  0.006895633618781516},
                                                                'volume': {'unit': 'l', 'value': 0}}]
    assert [x.sample.id for x in variant.reactants] == [3]
    assert [x.clean() for x in variant.reactants] == [{'amount': {'unit': 'mol', 'value': 0.0015901905481965069},
                                                       'aux': {'coefficient': 1.0,
                                                               'equivalent': 0.013540885967435785,
                                                               'isReference': False,
                                                               'loading': None,
                                                               'molarity': 0.0,
                                                               'molecularWeight': 13.83482,
                                                               'purity': 1.0,
                                                               'sumFormula': 'BH3',
                                                               'yield': None},
                                                       'mass': {'unit': 'g', 'value': 0.00011494118088970488},
                                                       'volume': {'unit': 'l', 'value': 0}}]
    assert [x.sample.id for x in variant.products] == [4]
    assert [x.clean() for x in variant.products] == [{'amount': {'unit': 'mol', 'value': 0.0},
                                                      'aux': {'coefficient': 1.0,
                                                              'equivalent': 0.0,
                                                              'isReference': False,
                                                              'loading': None,
                                                              'molarity': 0.0,
                                                              'molecularWeight': 142.19710000000003,
                                                              'purity': 1.0,
                                                              'sumFormula': 'C11H10',
                                                              'yield': 0.0},
                                                      'mass': {'unit': 'g', 'value': 0.0},
                                                      'volume': {'unit': 'l', 'value': 0}}]


def test_remove_variations(instance_with_test_reaction):
    ra = instance_with_test_reaction['instance'].get_reaction(1)
    v = ra.variations[-1]
    v_len = len(ra.variations)
    assert ra.variations.remove(v)
    ra.save(True)
    assert len(ra.variations) == v_len - 1
